import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Book, Image, Author } from '../shared/book';
import { BookStoreService } from "../shared/book-store.service";

@Component({
    selector: 'bs-book-list',
    templateUrl: './book-list.component.html',
    styles: []
})
export class BookListComponent implements OnInit {

    books: Book[];
    //EventEmitter in Angular Doc durchlesen, Jetzt kann die BookList Component ein Ereignis feuern
    // Dieses Ergeignis können dann andere Komponente dann behandeln
    // feuern müssen dieses Event erst dann wenn auf ein Buch geklickt wird
    //@Output() showDetailsEvent = new EventEmitter<Book>();

    constructor(private bs : BookStoreService) {}

    //automatisch aufgerufen wenn Komponente geladen wird
    ngOnInit() {
        this.bs.getAll().subscribe(res => this.books = res);

    }



}

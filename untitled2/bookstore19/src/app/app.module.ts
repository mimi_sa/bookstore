import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookListItemComponent } from './book-list-item/book-list-item.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { BookStoreService} from "./shared/book-store.service";
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from "./app-routing.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { BookFormComponent } from './book-form/book-form.component';
import { LoginComponent } from './admin/login/login.component';
import {AuthService} from "./shared/authentication.service";
import {TokenInterceptorService} from "./shared/token-interceptor.service";
import {JwtInterceptorService} from "./shared/jwt-interceptor.service";
import { OrderListComponent } from './order-list/order-list.component';
import { CardListComponent } from './card-list/card-list.component';
import { OwnOrderListComponent } from './own-order-list/own-order-list.component';
import {OrderService} from "./shared/order.service";
import { OrderFormComponent } from './order-form/order-form.component';
import { OrderspositionFormComponent } from './ordersposition-form/ordersposition-form.component';
import { StatusFormComponent } from './status-form/status-form.component'; [ OrderService]

@NgModule({
    declarations: [
        AppComponent,
        BookListComponent,
        BookListItemComponent,
        BookDetailsComponent,
        HomeComponent,
        BookFormComponent,
        LoginComponent,
        OrderListComponent,
        CardListComponent,
        OwnOrderListComponent,
        OrderFormComponent,
        OrderspositionFormComponent,
        StatusFormComponent
    ],
    imports: [
        BrowserModule, FormsModule, AppRoutingModule, HttpClientModule,
        ReactiveFormsModule
    ],
    providers: [
        BookStoreService, AuthService, OrderService, {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptorService,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptorService,
            multi: true
        }

    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

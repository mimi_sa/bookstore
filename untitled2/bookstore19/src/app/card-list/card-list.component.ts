import { Component, OnInit } from "@angular/core";
import { AuthService } from "./../shared/authentication.service";
import { Order } from "../shared/order";
import { OrderService } from "../shared/order.service";
import { Tax } from "../shared/tax";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "bs-card-list",
  templateUrl: "./card-list.component.html",
  styles: []
})
export class CardListComponent implements OnInit {
  tax: Tax;

  constructor(
    private os: OrderService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.os.getTax().subscribe(res => (this.tax = res));
  }

  exist() {
    var restoredBooks = JSON.parse(localStorage.getItem("books"));
    if (
      Object.keys(restoredBooks.queue).length != 0 &&
      localStorage.getItem("books")
    ) {
      return true;
    }
    return false;
  }

  getBooks() {
    var restoredBooks = JSON.parse(localStorage.getItem("books"));
    return restoredBooks.queue;
  }

  getNetto() {
    var netto = 0;
    var books = this.getBooks();

    if (books && books[0]) {
      for (let book of books) {
        netto += Number(book.price * book.amount);
      }
    }
    return Number(netto).toFixed(2);
  }

  getTax() {
    var tax = 0;
    if (this.isLoggedIn()) {
      tax = this.tax.brutto;
      return Number(tax).toFixed(0);
    }
    return 10;
  }

  getBrutto() {
    var netto = 0;
    netto = Number(this.getNetto());
    var tax = 0;
    tax = Number(this.getTax());
    var brutto = netto * (1 + tax / 100);
    return Number(brutto).toFixed(2);
  }

  isLoggedIn() {
    return this.authService.isLoggedIn();
  }

  delete() {
    var node_list = document.getElementsByTagName("input");
    var restoredBooks = JSON.parse(localStorage.getItem("books"));
    for (var i = 0; i < node_list.length; i++) {
      var node = node_list[i];
      if (node.getAttribute("type") == "checkbox") {
        if (node.checked) {
          var x = 0;
          for (let book of restoredBooks.queue) {
            if (book.id == node.nextSibling.textContent) {
              restoredBooks.queue.splice(x, 1);
            }
            x++;
          }
        }
      }
    }
    localStorage.setItem("books", JSON.stringify(restoredBooks));
  }

  bestellen() {
    let user = JSON.parse(localStorage.getItem("userId"));
    let orderpositions = [];

    var node_list = document.getElementsByTagName("input");
    var restoredBooks = JSON.parse(localStorage.getItem("books"));
    for (var i = 0; i < node_list.length; i++) {
      var node = node_list[i];
      if (node.getAttribute("type") == "checkbox") {
        if (node.checked) {
          var x = 0;
          for (let book of restoredBooks.queue) {
            if (book.id == node.nextSibling.textContent) {
              orderpositions.push({
                id: 100,
                amount: book.amount,
                book_id: book.id,
                user_id: user
              });

              restoredBooks.queue.splice(x, 1);
            }
            x++;
          }
        }
      }
    }

    const order: Order = {
      id: 100,
      user_id: user,
      statuses: [
        {
          id: 100,
          bezeichnung: "offen",
          kommentar: null,
          order_id: 100
        }
      ],
      ordersposition: orderpositions,
      brutto: this.getTax()
    };

    localStorage.setItem("books", JSON.stringify(restoredBooks));
    this.os
      .create(order)
      .subscribe(res =>
        this.router.navigate(["../myorders"], { relativeTo: this.route })
      );
  }

  add(bookObject) {
    var restoredBooks = JSON.parse(localStorage.getItem("books"));
    for (let book of restoredBooks.queue) {
      if (book.id == bookObject.id) {
        book.amount++;
      }
    }
    localStorage.setItem("books", JSON.stringify(restoredBooks));
  }

  minus(bookObject) {
    var restoredBooks = JSON.parse(localStorage.getItem("books"));
    for (let book of restoredBooks.queue) {
      if (book.id == bookObject.id) {
        if (book.amount > 1) {
          book.amount--;
        }
      }
    }
    localStorage.setItem("books", JSON.stringify(restoredBooks));
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { Order } from '../shared/order';
import { Book } from '../shared/book';
import { Status } from '../shared/status';
import { OrderService } from "../shared/order.service";
import { BookStoreService } from "../shared/book-store.service";
import { User } from "../shared/user";

@Component({
  selector: 'bs-order-list',
  templateUrl: './order-list.component.html',
  styles: []
})
export class OrderListComponent implements OnInit {

    @Input() status: Status;

  orders: Order[];
    savedOrders: Order[];
    sorted = false;
  books: Book[];
  users: User[];

  constructor(private os: OrderService, private bs: BookStoreService) { }

  ngOnInit() {
    this.os.getAll().subscribe(res => this.orders = res);
      this.os.getUsers().subscribe(res => this.users = res);
    this.bs.getAll().subscribe(res => this.books = res);
  }

    getPrice(id: number) {
        if(this.books && this.books[0]) {
            for (let book of this.books) {
                if (book.id == id) {
                    return book.price;
                }
            }
        }
        return 0;
    }

    getPriceWithAmount(id: number, amount: number) {
        if(this.books && this.books[0]) {
            for (let book of this.books) {
                if (book.id == id) {
                    return (book.price * amount).toFixed(2);
                }
            }
        }
        return 0;
    }

    getNetto(id: number) {
        var orderspositions = null;
        if(this.orders && this.orders[0]) {
            for (let order of this.orders) {
                if (order.id == id) {
                    orderspositions = order.ordersposition;
                }
            }
        }
        var netto = 0;
        for (let pos of orderspositions) {
            netto += Number(this.getPriceWithAmount(pos.book_id, pos.amount));
        }
        return Number(netto).toFixed(2);
    }

  getBrutto(id: number) {
    var netto = Number(this.getNetto(id));
    var mwst = 0;
    if(this.orders && this.orders[0]) {
        for (let order of this.orders) {
            if (order.id == id) {
                mwst = Number(order.brutto);
            }
        }
    }
    return Number(netto * (1 + (mwst / 100))).toFixed(2);
  }

    getLastStatus(id: number) {
        if(this.orders && this.orders[0]) {
            for (let order of this.orders) {
                if (order.id == id) {
                    return order.statuses[order.statuses.length - 1].bezeichnung;
                }
            }
        }
    }

    send(id: number) {
        const status: Status = {
            "id": 100,
            "bezeichnung": "versendet",
            "order_id": id
        };

        this.os.saveStatus(status).subscribe(res => location.reload());
    }

    editStatus(status) {
        var kommentar = prompt("Ändern sie den Kommentar", `${status.kommentar ? status.kommentar : ''}`);

      this.status = {
          "id": status.id,
          "bezeichnung": status.bezeichnung,
          "kommentar": kommentar,
          "order_id": status.order
      }

        this.os.updateStatus(this.status).subscribe(res => location.reload());
    }

    sortIdAsc () {
      this.os.ordersIdAsc().subscribe(res => location.reload());
    }

    sortIdDesc () {
        this.os.ordersIdDesc().subscribe(res => location.reload());
    }

    sortDateAsc () {
        this.os.sortOrderDateAsc().subscribe(res => location.reload());
    }

    sortDateDesc () {
        this.os.sortOrderDateDesc().subscribe(res => location.reload());
    }

    sortStatus (word: string) {
      if (this.sorted) {
          this.orders = this.savedOrders;
      } else {
          this.savedOrders = this.orders;
      }

        let sortedOrders = [];
        for (let order of this.orders) {
            if (order.statuses[order.statuses.length - 1].bezeichnung == word) {
                sortedOrders.push(order);
            }

            /*for (let status of order.statuses) {
                if (status.bezeichnung == word) {
                    sortedOrders.push(order);
                }
            }*/
        }
        this.sorted = true;
        this.orders = sortedOrders;
    }

    sortWithoutStatus () {
      if (this.sorted) {
          this.orders = this.savedOrders;
      }
    }

    getTitle (id: number) {
        var title = '-';
        if (this.books && this.books[0] ) {
            for (let book of this.books) {
                if (book.id == id) {
                    if(book.title) {
                        title = book.title;
                    }
                }
            }
        }
        return title;
    }

    getSubtitle (id: number) {
        var subtitle = '-';
        if (this.books && this.books[0] ) {
            for (let book of this.books) {
                if (book.id == id) {
                    if(book.subtitle) {
                        subtitle = book.subtitle;
                    }
                }
            }
        }
        return subtitle;
    }

    getISBN (id: number) {
        var isbn = '-';
        if (this.books && this.books[0] ) {
            for (let book of this.books) {
                if (book.id == id) {
                    if(book.isbn) {
                        isbn = book.isbn;
                    }
                }
            }
        }
        return isbn;
    }

    getAddress (id: number) {
      if (this.users && this.users[0]) {
          for (let user of this.users) {
              if (user.id == id && user.address.country && user.address.location && user.address.street) {
                  return user.address.country + ' - ' + user.address.location + ', ' + user.address.street;
              }
          }
      }
    }
}

import { Status } from "./status";
export { Status } from "./status";
import { Ordersposition } from "./ordersposition";
export { Ordersposition} from "./ordersposition";

export class Order {
    constructor (
        public id: number,
        public user_id: number,
        public brutto: string | number,
        public statuses? : Status[],
        public ordersposition? : Ordersposition[]
    ) {}
}

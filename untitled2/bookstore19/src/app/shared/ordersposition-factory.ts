import { Ordersposition } from './ordersposition';

export class OrderspositionFactory {

    static empty(): Ordersposition {
        return new Ordersposition(null, 0, null, null);
    }

    static fromObject(rawOrdp: any): Ordersposition {
        return new Ordersposition(
            rawOrdp.id,
            rawOrdp.amount,
            rawOrdp.book_id,
            rawOrdp.order_id
        );
    }
}

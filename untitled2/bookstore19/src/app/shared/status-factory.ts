import { Status } from './status';

export class StatusFactory {

    static empty(): Status {
        return new Status(null, '', null, '');
    }

    // generiert aus den übergebenen Werten wieder ein Buchobjekt
    static fromObject(rawStatus: any): Status {
        return new Status(
            rawStatus.id,
            rawStatus.bezeichnung,
            rawStatus.order_id,
            rawStatus.kommentar
        );
    }
}

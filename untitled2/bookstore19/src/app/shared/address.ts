export class Address {
    constructor (
        public id: number,
        public country: string,
        public location: string,
        public street: string,
        public plz? : string
    ) { }
}

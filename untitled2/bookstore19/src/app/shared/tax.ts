export class Tax {
    constructor (
       public id: number,
       public country: string,
       public brutto: number
    ) {}
}

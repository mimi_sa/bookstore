import { Injectable } from "@angular/core";
import { isNullOrUndefined } from "util";
import { HttpClient } from "@angular/common/http";
import * as decode from "jwt-decode";
import { retry } from "rxjs/internal/operators";

//npm install --save-dev jwt-decode

interface User {
  result: {
    created_at: Date;
    email: string;
    id: number;
    lastName: string;
    firstName: string;
    birthday: Date;
    admin_flag: boolean;
    updated_at: Date;
  };
}

@Injectable()
export class AuthService {
  //wir instanzieren in dem moment wo wir authservice verwenden eine instanz

  private api: string =
    "http://bookstore19.s1610456029.student.kwmhgb.at/api/auth"; //'http://localhost:8080/api/auth';

  constructor(private http: HttpClient) {}

  //schickt restcall ab (loginmethode)
  login(email: string, password: string) {
    return this.http.post(`${this.api}/login`, {
      email: email,
      password: password
    });
  }

  public setCurrentUserId() {
    //wir holen uns benutzer aus restservice u speichern benutzer bei uns am localstorage
    this.http
      .get<User>(`${this.api}/user`)
      .pipe(retry(3))
      .subscribe(res => {
        localStorage.setItem("userId", res.result.id.toString());
        localStorage.setItem("admin", res.result.admin_flag.toString());
        localStorage.setItem("lastName", res.result.lastName.toString());
        localStorage.setItem("firstName", res.result.firstName.toString());
      });
  }

  public getCurrentUserId() {
    //holt daten von localstorage
    return Number.parseInt(localStorage.getItem("userId"));
  }

  public getAdminFlag() {
    return localStorage.getItem("admin");
  }

  public setLocalStorage(token: string) {
    /*console.log("Storing token");
        console.log(token);*/
    const decodedToken = decode(token);
    /*console.log(decodedToken);
        console.log(decodedToken.user.id);*/
    localStorage.setItem("token", token);
    localStorage.setItem("userId", decodedToken.user.id);
    localStorage.setItem("lastName", decodedToken.user.lastName);
    localStorage.setItem("firstName", decodedToken.user.firstName);
    localStorage.setItem("admin", decodedToken.user.admin_flag);
  }

  public logout() {
    //killt aktuellen token. alles was ich im localstorage habe muss gelöscht werden
    this.http.post(`${this.api}/logout`, {});
    localStorage.removeItem("token");
    localStorage.removeItem("userId");
    localStorage.removeItem("lastName");
    localStorage.removeItem("firstName");
    localStorage.removeItem("admin");
  }

  public isLoggedIn() {
    //wird nachgesehen ob es einen token gibt oder nicht
    return !isNullOrUndefined(localStorage.getItem("token"));
  }

  public isLoggedOut() {
    //wird nachgesehen ob er aktuell ausgeloggt ist
    return !this.isLoggedIn();
  }
}

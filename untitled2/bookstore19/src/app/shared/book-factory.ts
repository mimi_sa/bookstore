import { Book } from './book';

export class BookFactory {

    static empty(): Book {
        return new Book(null, '', '', [], new Date(),0,false, '', 0, [{id: 0, url: '', title: ''}], '');
    }

    // generiert aus den übergebenen Werten wieder ein Buchobjekt
    static fromObject(rawBook: any): Book {
        return new Book(
            rawBook.id,
            rawBook.isbn,
            rawBook.title,
            rawBook.authors,
            typeof(rawBook.published) === 'string' ?
                new Date(rawBook.published) : rawBook.published,
            rawBook.price,
            rawBook.invisible,
            rawBook.subtitle,
            rawBook.rating,
            rawBook.images,
            rawBook.description
        );
    }
}
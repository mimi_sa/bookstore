import { Order } from './order';

export class OrderFactory {

    static empty(): Order {
        return new Order(null, 0, null);
    }

    static fromObject(rawOrder: any): Order {
        return new Order(
            rawOrder.id,
            rawOrder.brutto,
            rawOrder.user_id
        );
    }
}

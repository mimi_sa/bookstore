export class Status {
    constructor (
       public id: number,
       public bezeichnung: string,
       public order_id: number,
       public kommentar?: string
    ) {}
}

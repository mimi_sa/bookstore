import { Book } from "./book";
export { Book } from "./book";
import { Order } from "./order";
export { Order } from "./order";

export class Ordersposition {
    constructor (
       public id: number,
       public amount: number,
       public book: Book,
       public order: Order
    ) {}
}

import { Address } from "./address";
export { Address } from "./address";

export class User {
    constructor (
        public id: number,
        public lastName: string,
        public firstName: string,
        public address: Address
    ) {}
}

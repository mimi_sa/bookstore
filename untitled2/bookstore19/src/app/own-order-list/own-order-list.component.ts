import { Component, OnInit, Input } from '@angular/core';
import { Order } from '../shared/order';
import { Book } from '../shared/book';
import {ActivatedRoute, Router} from "@angular/router";
import { OrderService } from "../shared/order.service";
import { BookStoreService } from "../shared/book-store.service";
import { Status } from '../shared/status';
import {Ordersposition} from "../shared/ordersposition";
import {User} from "../shared/user";

@Component({
  selector: 'bs-own-order-list',
  templateUrl: './own-order-list.component.html',
  styles: []
})
export class OwnOrderListComponent implements OnInit {

    @Input() order: Order;
    @Input() ordersposition: Ordersposition;

    orders: Order[];
    books: Book[];
    users: User[];

  constructor(private os: OrderService, private bs: BookStoreService, private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
      this.os.getOwn().subscribe(res => this.orders = res);
      this.os.getUsers().subscribe(res => this.users = res);
      this.bs.getAll().subscribe(res => this.books = res);
  }

    getPrice(id: number) {
      if(this.books && this.books[0]) {
          for (let book of this.books) {
              if (book.id == id) {
                  return book.price;
              }
          }
          return 0;
      }
    }

    getPriceWithAmount(id: number, amount: number) {
        if(this.books && this.books[0]) {
            for (let book of this.books) {
                if (book.id == id) {
                    return (book.price * amount).toFixed(2);
                }
            }
            return 0;
        }
    }

    getNetto(id: number) {
        var orderspositions: any = [];
        if(this.orders && this.orders[0]) {
            for (let order of this.orders) {
                if (order.id == id) {
                    orderspositions = order.ordersposition;
                }
            }
        }
        var netto = 0;
        if(orderspositions && orderspositions[0]) {
            for (let pos of orderspositions) {
                let x = Number(this.getPriceWithAmount(pos.book_id, pos.amount));
                netto += x;
            }
        }
        return Number(netto).toFixed(2);
    }

    getBrutto(id: number) {
        var netto = Number(this.getNetto(id));
        var mwst = 0;
        if(this.orders && this.orders[0]) {
            for (let order of this.orders) {
                if (order.id == id) {
                    mwst = Number(order.brutto);
                }
            }
        }
        return Number(netto * (1 + (mwst / 100))).toFixed(2);
    }

    getLastStatus(id: number) {
        if(this.orders && this.orders[0]) {
            for (let order of this.orders) {
                if (order.id == id) {
                    return order.statuses[order.statuses.length - 1].bezeichnung;
                }
            }
        }
    }

    bezahlen(id: number) {
      const status: Status = {
          "id": 100,
          "bezeichnung": "bezahlt",
          "order_id": id
      };

      this.os.saveStatus(status).subscribe(res => location.reload());
    }

    removeOrder(id: number) {
        if (confirm("Bestellung wirklich löschen?")) {
            this.os.removeOrder(id)
                .subscribe(res => location.reload());
        }
    }

    storno(id: number) {
        const status: Status = {
            "id": 100,
            "bezeichnung": "storniert",
            "order_id": id
        };

        this.os.saveStatus(status).subscribe(res => location.reload());
    }

    editOrdersposition(ordersposition) {
        var amount = prompt("Stückzahl ändern", `${ordersposition.amount ? ordersposition.amount : '0'}`);

        this.ordersposition = {
          "id": ordersposition.id,
          "amount": Number(amount),
            "book": ordersposition.book_id,
            "order": ordersposition.order_id
      }

      this.os.updateOrdersposition(this.ordersposition).subscribe(res => location.reload());
    }

    deleteOrdersposition(ordersposition) {
        var oneInOrder = false;
        for (let order of this.orders) {
            if (order.id == ordersposition.order_id) {
                if (order.ordersposition.length == 1) {
                    this.removeOrder(order.id);
                    oneInOrder = true;
                }
            }
        }

        if (!oneInOrder) {
            if (confirm("Bestellposition wirklich löschen?")) {
                this.os.removeOrdersposition(ordersposition.id)
                    .subscribe(res => location.reload());
            }
        }
    }

    getTitle (id: number) {
      var title = '-';
      if (this.books && this.books[0] ) {
          for (let book of this.books) {
              if (book.id == id) {
                  if(book.title) {
                      title = book.title;
                  }
              }
          }
      }
      return title;
    }

    getSubtitle (id: number) {
        var subtitle = '-';
        if (this.books && this.books[0] ) {
            for (let book of this.books) {
                if (book.id == id) {
                    if(book.subtitle) {
                        subtitle = book.subtitle;
                    }
                }
            }
        }
        return subtitle;
    }

    getISBN (id: number) {
        var isbn = '-';
        if (this.books && this.books[0] ) {
            for (let book of this.books) {
                if (book.id == id) {
                    if(book.isbn) {
                        isbn = book.isbn;
                    }
                }
            }
        }
        return isbn;
    }

    getAddress (id: number) {
        if (this.users && this.users[0]) {
            for (let user of this.users) {
                if (user.id == id && user.address.country && user.address.location && user.address.street) {
                    return user.address.country + ' - ' + user.address.location + ', ' + user.address.street;
                }
            }
        }
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnOrderListComponent } from './own-order-list.component';

describe('OwnOrderListComponent', () => {
  let component: OwnOrderListComponent;
  let fixture: ComponentFixture<OwnOrderListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnOrderListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnOrderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

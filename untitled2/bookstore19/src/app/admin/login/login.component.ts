import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Router} from "@angular/router";
import { AuthService } from "../../shared/authentication.service";

interface Response {
    response : string;
    result: {
        token: string;
    }
}

@Component({
    selector: 'bs-login',
    templateUrl: './login.component.html',
    styles: []
})
export class LoginComponent implements OnInit {

    loginForm : FormGroup;

    constructor( private fb: FormBuilder, private router: Router,
                 private authService: AuthService) { }



    ngOnInit() {
        this.loginForm = this.fb.group({
            username: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }

    login() {
        //wenn ich beim formular auf abschicken drucke wird diese methode aufgerufen
        //ich übermittle username u pw an authentification service
        const val = this.loginForm.value;
        if (val.username && val.password) {
            //authservice wird aufgerufen. wenn username/pw an restservice geschickt wurde, kann ich schaun ob
            // wir eingeloggt sind od nicht
            this.authService.login(val.username, val.password).subscribe(res => {
                const resObj = res as Response; //mach typemapping
                if (resObj.response === 'success') {
                    //wenn key success ist (wntwort json zb postman)
                    this.authService.setLocalStorage(resObj.result.token); //local storage erwartet token
                    this.router.navigateByUrl(('/'));
                }
            })
        }
    }

    isLoggedIn() {
        return this.authService.isLoggedIn();
    }

    logout() {
        this.authService.logout();
    }

}

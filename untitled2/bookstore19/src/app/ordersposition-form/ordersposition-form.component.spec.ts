import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderspositionFormComponent } from './ordersposition-form.component';

describe('OrderspositionFormComponent', () => {
  let component: OrderspositionFormComponent;
  let fixture: ComponentFixture<OrderspositionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderspositionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderspositionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

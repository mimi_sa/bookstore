import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../shared/authentication.service'
import {BookStoreService} from "../shared/book-store.service";
import { Book, Image, Author } from '../shared/book';

@Component({
    selector: 'bs-home',
    template: `
        <h1 class="ui green center aligned header">
            Willkommen{{welcome()}}! 
        </h1>
        <div class="ui divider"></div>
        <div class="ui grid">
            <div class="sixteen wide column">
            <h2 class="ui center aligned header">
                Das sind die Top-Angebote der Woche...
            </h2>
            </div>
            <div class="equal centered width row">
                <div *ngFor="let book of top" class="ui medium images" style="margin: 1rem">
                    <img *ngIf="book.images && book.images[0]"
                        [src]="book.images[0].url" [routerLink]="['../books', book.isbn]">
                </div>
            </div>
            <div class="ui divider"></div>
            <div class="sixteen wide column">
            <h2 class="ui center aligned header">
                Alles von Game of Thrones
            </h2>
            </div>
            <div class="equal centered width row">
                <div *ngFor="let book of got" class="ui small images" style="margin: 1rem">
                    <img *ngIf="book.images && book.images[0]"
                         [src]="book.images[0].url" [routerLink]="['../books', book.isbn]">
                </div>
            </div>
            <div class="ui divider"></div>
            <div class="sixteen wide column">
            <h2 class="ui center aligned header">
                Alles von Harry Potter
            </h2>
            </div>
            <div class="equal centered width row">
                <div *ngFor="let book of harry" class="ui small images" style="margin: 1rem">
                    <img *ngIf="book.images && book.images[0]"
                         [src]="book.images[0].url" [routerLink]="['../books', book.isbn]">
                </div>
            </div>


            <div *ngIf="!top" class="ui active dimmer">
                <div class="ui large text loader">Daten werden geladen...</div>
            </div>
        </div>
    `,
    styles: []
})
export class HomeComponent implements OnInit {

    top: Book[];
    got: Book[];
    harry: Book[];

    constructor(private bs : BookStoreService) { }

    ngOnInit() {
        this.bs.searchBook("erst").subscribe(res => this.top = res);
        this.bs.searchBook("george").subscribe(res => this.got = res);
        this.bs.searchBook("harry").subscribe(res => this.harry = res);
    }

    welcome() {
        return localStorage.getItem('firstName') != null && localStorage.getItem('lastName') != null ? (" " + localStorage.getItem('firstName') + " " + localStorage.getItem('lastName')) : "";
    }

}

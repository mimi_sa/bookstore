<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    protected $fillable = [
        'brutto', 'user_id'
    ];

    /**
     * order has many ordersposition
     */
    public function ordersposition() : HasMany {
        return $this->hasMany(Ordersposition::class);
    }

    /**
     * order has many statuses
     */
    public function statuses() : HasMany {
        return $this->hasMany(Status::class);
    }

    public function user() : BelongsTo  {
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Book;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lastName', 'firstName', 'email', 'birthday', 'password', 'admin_flag'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * user has one address
     */
    public function address() : HasOne {
        return $this->hasOne(Address::class);
    }

    /**
     * user has many orders
     */
    public function orders() : HasMany {
        return $this->hasMany(Order::class);
    }

    public static function saveBrutto() {
        $users = User::with(['orders', 'address'])->get();

        foreach ($users as $user) {
            //when order nicht leer
            if ($user->orders->first() != null) {
                $user->address->country; //country
                //gehe taxes durch und speichere passenden brutto wert
                $taxes = DB::table('taxes')->where('country', $user->address->country)->select('brutto')->first();
                //speichere es in order
                foreach ($user->orders as $order) {
                    $order->brutto = $taxes;
                }
            }

        }
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return ['user' => ['id' => $this->id,
            'admin_flag' => $this->admin_flag,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName],
            ];
    }

}

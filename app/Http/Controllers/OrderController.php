<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

use App\Order;
use App\Ordersposition;
use App\Tax;
use App\Status;
use App\Book;
use JWTAuth;

class OrderController extends Controller
{
    public function getAllTaxes() {
        return Tax::get();
    }

    public function saveTax (Request $request) : JsonResponse {
        $request = $this->parseRequest($request);
        DB::beginTransaction();
        try {
            $tax = Tax::create($request->all());
            DB::commit();
            return response()->json($tax, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json('saving tax failed' . $e->getMessage(), 420);
        }
    }

    public function updateTax(Request $request, string $id) : JsonResponse {
        DB::beginTransaction();
        try {
            $tax = Tax::where('id', $id)->first();
            if ($tax != null) {
                $request = $this->parseRequest($request);
                $tax->update($request->all());
            }

            DB::commit();
            $tax1 = Tax::where('id', $id)->first();
            return response()->json($tax1, 201);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating tax failed: " . $e->getMessage(), 420);
        }
    }

    public function saveStatus (Request $request) : JsonResponse {
        $request = $this->parseRequest($request);
        DB::beginTransaction();
        try {
            $status = Status::create($request->all());

            if ($request['orders'] && is_array($request['orders'])) {
                foreach ($request['orders'] as $o) {
                    $order = Order::firstOrNew([
                            'id' => $o['order_id'],
                        ]
                    );
                    $status->orders()->save($order);
                }
            }

            DB::commit();
            return response()->json($status, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json('saving status failed' . $e->getMessage(), 420);
        }
    }

    public function updateStatus(Request $request, string $id) : JsonResponse {
        DB::beginTransaction();
        try {
            $stat = Status::where('id', $id)->first();
            if ($stat != null) {
                $request = $this->parseRequest($request);
                $stat->update($request->all());
            }

            DB::commit();
            $stat1 = Status::where('id', $id)->first();
            return response()->json($stat1, 201);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating status failed: " . $e->getMessage(), 420);
        }
    }

    //alle orders anzeigen mit status
    public function getAllOrders() {
        $orders = Order::with(['statuses', 'ordersposition', 'user'])
            ->orderBy('created_at', 'desc')->get();
        return $orders;
    }

    //bestimmte order suchen oder filtern nach
    public function findOrders(int $id, int $orderId, string $searchTerm) {
        $orders = Order::with(['statuses', 'ordersposition'])
            ->where('id', $id)
            ->orWhere('order_id', $orderId)
            ->orWhereHas('statuses', function($query) use ($searchTerm) {
                $query->where('bezeichnung', 'LIKE', '%' . $searchTerm . '%');
            })->get();
        return $orders;
    }

    //eigene orders
    public function getOwnOrders() {
        $user = JWTAuth::user();
        $orders = Order::with(['statuses', 'ordersposition'])
            ->where('user_id', $user->id)
            ->orderBy('created_at', 'desc')->get();
        return $orders;
    }

    public function sortAscAllOrders(string $sort) {
        $orders = Order::with(['statuses', 'ordersposition', 'user'])
            ->orderBy($sort, 'asc')->get();
        return $orders;
    }

    public function sortDescAllOrders(string $sort) {
        $orders = Order::with(['statuses', 'ordersposition', 'user'])
            ->orderBy($sort, 'desc')->get();
        return $orders;
    }


    public function sortDateAscAllOrders() {
        $orders = Order::with(['statuses', 'ordersposition', 'user'])
            ->orderByRaw('created_at', 'asc')->get();
        return $orders;
    }

    public function sortDateDescAllOrders() {
        $orders = Order::with(['statuses', 'ordersposition', 'user'])
            ->orderByRaw('created_at', 'desc')->get();
        return $orders;
    }

    public function sortIdAscAllOrders() {
        $orders = Order::with(['statuses', 'ordersposition', 'user'])
            ->orderByRaw('user_id', 'asc')->get();
        return $orders;
    }

    public function sortIdDescAllOrders() {
        $orders = Order::with(['statuses', 'ordersposition', 'user'])
            ->orderByRaw('user_id', 'desc')->get();
        return $orders;
    }

    //ordersposition ausgeben
    public function getOrdersposition(int $id) {
        $ordersposition = Ordersposition::with(['book'])
            ->where('order_id', $id)->get();
        return$ordersposition;
    }

    public function saveOrderposition(Request $request) : JsonResponse {
        $request = $this->parseRequest($request);
        DB::beginTransaction();
        try {
            $odpos = Ordersposition::create($request->all());

            if ($request['book'] && is_array($request['book'])) {
                foreach ($request['book'] as $o) {
                    $book = Book::firstOrNew([
                            'id' => $o['book_id'],
                        ]
                    );
                    $odpos->orders()->save($book);
                }
            }

            if ($request['orders'] && is_array($request['orders'])) {
                foreach ($request['orders'] as $o) {
                    $order = Order::firstOrNew([
                            'id' => $o['order_id'],
                        ]
                    );
                    $odpos->orders()->save($order);
                }
            }

            DB::commit();
            return response()->json($odpos, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json('saving orderposition failed' . $e->getMessage(), 420);
        }
    }

    public function updateOrderposition(Request $request, string $id) : JsonResponse {
        DB::beginTransaction();
        try {
            $odpos = Ordersposition::where('id', $id)->first();
            if ($odpos != null) {
                $request = $this->parseRequest($request);
                $odpos->update($request->all());
            }

            DB::commit();
            $odpos1 = Status::where('id', $id)->first();
            return response()->json($odpos1, 201);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating orderposition failed: " . $e->getMessage(), 420);
        }
    }


    //je order ausrechnen, bestellporitionen netto, gesamt netto, gesamt brutto

    //alle eigenen orders geammt netto und gesamt brutto

    public function getTax () {
        $user = JWTAuth::user();
        return Tax::where('country', $user->address->country)->select('brutto')->first();
    }


    public function saveOrder (Request $request) : JsonResponse {
        $user = JWTAuth::user();
        $taxes = Tax::where('country', $user->address->country)->select('brutto')->first();

        $request = $this->parseRequest($request);
        DB::beginTransaction();
        try {
            $order = Order::create($request->all());
            $order->update(['brutto' => $taxes->value('brutto')]);

            // save status
            if ($request['statuses'] && is_array($request['statuses'])) {
                foreach ($request['statuses'] as $stats) {
                    $status = Status::firstOrNew([
                        'bezeichnung' => $stats['bezeichnung'],
                        'kommentar' => $stats['kommentar']
                    ]);
                    $order->statuses()->save($status);
                }
            }

            //save ordersposition
            if ($request['ordersposition'] && is_array($request['ordersposition'])) {
                foreach ($request['ordersposition'] as $orpos) {
                    $ordersposition = Ordersposition::firstOrNew([
                        'amount' => $orpos['amount'],
                        'book_id' => $orpos['book_id']
                    ]);
                    $order->ordersposition()->save($ordersposition);
                }
            }

            DB::commit();
            return response()->json($order, 201);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json('saving order failed ' . $e->getMessage(), 420);
        }
    }

    public function updateOrder(Request $request, string $id) : JsonResponse {
        DB::beginTransaction();
        try {
            $order = Order::with(['statuses', 'ordersposition'])
                ->where('id', $id)->first();
            if ($order != null) {
                $request = $this->parseRequest($request);
                $order->update($request->all());

                //delete status???
                $order->statuses()->delete();
                //save statuses
                if (isset($request['statuses']) && is_array($request['statuses'])) {
                    foreach ($request['statuses'] as $sts) {
                        $status = Status::firstOrNew(['bezeichnung' => $sts['bezeichnung'], 'kommentar' => $sts['kommentar']]);
                        $order->statuses()->save($status);
                    }
                }

                //delete ordersposition
                $order->ordersposition()->delete();
                if (isset($request['ordersposition']) && is_array($request['ordersposition'])) {
                    foreach ($request['ordersposition'] as $pos) {
                        $position = Ordersposition::firstOrNew(['amount' => $pos['amount'], 'book_id' => $pos['book_id']]);
                        $order->ordersposition()->save($position);
                    }
                }
            }

            DB::commit();
            $order1 = Order::with(['statuses', 'ordersposition'])->where('id', $id)->first();
            return response()->json($order1, 201);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating order failed: " . $e->getMessage(), 420);

        }
    }

    public function deleteOrder(int $orderId) : JsonResponse {
        $order = Order::where('id', $orderId)->first();
        if ($order != null) {
            $order->delete();
        }
        else {
            throw new \Exception("order couldn't be deleted - does not exist");
        }
        return response()->json('order deleted', 200);
    }

    public function deleteOrdersposition(int $id) : JsonResponse {
        $ordersposition = Ordersposition::where('id', $id)->first();
        if ($ordersposition != null) {
            $ordersposition->delete();
        }
        else {
            throw new \Exception("ordersposition couldn't be deleted - does not exist");
        }
        return response()->json('ordersposition deleted', 200);
    }

    public function deleteStatus(int $id) : JsonResponse {
        $status = Status::where('id', $id)->first();
        if ($status != null) {
            $status->delete();
        }
        else {
            throw new \Exception("status couldn't be deleted - does not exist");
        }
        return response()->json('status deleted', 200);
    }

    private function parseRequest (Request $request) : Request {
        $date = new \DateTime($request->published);
        $request['published'] = $date;
        return $request;
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Ordersposition extends Model
{
    protected $fillable = [
        'amount', 'book_id', 'order_id'
    ];

    public function order() : BelongsTo  {
        return $this->belongsTo(Order::class);
    }

    public function book() : BelongsTo  {
        return $this->belongsTo(Book::class);
    }
}

<?php

use Illuminate\Database\Seeder;

class TaxesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order = App\Order::all()->first();


        $t1 = new \App\Tax();
        $t1->country = "AT";
        $t1->brutto = "10";
        /*$t1->order()->associate($order);*/
        $t1->save();

        $t2 = new \App\Tax();
        $t2->country = "DE";
        $t2->brutto = "7";
        /*$t2->order()->associate($order);*/
        $t2->save();

        $t3 = new \App\Tax();
        $t3->country = "TO";
        $t3->brutto = "20";
        /*$t3->order()->associate($order);*/
        $t3->save();
    }
}

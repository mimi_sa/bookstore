<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = App\User::all()->get('1');
        $user2 = App\User::all()->get('2');
        $user3 = App\User::all()->get('3');
        $user4 = App\User::all()->get('4');

        $o1 = new \App\Order();
        $o1->brutto = 7;
        $o1->user()->associate($user1);
        $o1->save();

        $o2 = new \App\Order();
        $o2->brutto = 7;
        $o2->user()->associate($user1);
        $o2->save();

        $o3 = new \App\Order();
        $o3->brutto = 10;
        $o3->user()->associate($user2);
        $o3->save();

        $o4 = new \App\Order();
        $o4->brutto = 10;
        $o4->user()->associate($user2);
        $o4->save();

        $o5 = new \App\Order();
        $o5->brutto = 10;
        $o5->user()->associate($user3);
        $o5->save();

        $o6 = new \App\Order();
        $o6->brutto = 10;
        $o6->user()->associate($user3);
        $o6->save();

        $o7 = new \App\Order();
        $o7->brutto = 10;
        $o7->user()->associate($user3);
        $o7->save();

        $o8 = new \App\Order();
        $o8->brutto = 10;
        $o8->user()->associate($user4);
        $o8->save();

        $o9 = new \App\Order();
        $o9->brutto = 10;
        $o9->user()->associate($user4);
        $o9->save();

        $o10 = new \App\Order();
        $o10->brutto = 10;
        $o10->user()->associate($user4);
        $o10->save();




        $status1 = new App\Status();
        $status1->bezeichnung = 'offen';
        $status1->order()->associate($o1);
        $status1->save();

        $status2 = new App\Status();
        $status2->bezeichnung = 'offen';
        $status2->order()->associate($o2);
        $status2->save();

        $status3 = new App\Status();
        $status3->bezeichnung = 'bezahlt';
        $status3->kommentar = 'mit PaySafe Card bezahlt';
        $status3->order()->associate($o2);
        $status3->save();

        $status5 = new App\Status();
        $status5->bezeichnung = 'offen';
        $status5->order()->associate($o3);
        $status5->save();

        $status6 = new App\Status();
        $status6->bezeichnung = 'offen';
        $status6->order()->associate($o4);
        $status6->save();

        $status7 = new App\Status();
        $status7->bezeichnung = 'storniert';
        $status7->kommentar = 'falsches Buch';
        $status7->order()->associate($o4);
        $status7->save();

        $status8 = new App\Status();
        $status8->bezeichnung = 'offen';
        $status8->order()->associate($o5);
        $status8->save();

        $status9 = new App\Status();
        $status9->bezeichnung = 'offen';
        $status9->order()->associate($o6);
        $status9->save();

        $status10 = new App\Status();
        $status10->bezeichnung = 'offen';
        $status10->order()->associate($o7);
        $status10->save();

        $status11 = new App\Status();
        $status11->bezeichnung = 'offen';
        $status11->order()->associate($o8);
        $status11->save();

        $status12 = new App\Status();
        $status12->bezeichnung = 'offen';
        $status12->order()->associate($o9);
        $status12->save();

        $status13 = new App\Status();
        $status13->bezeichnung = 'offen';
        $status13->kommentar = 'derzeit nicht im Lager';
        $status13->order()->associate($o10);
        $status13->save();
    }
}

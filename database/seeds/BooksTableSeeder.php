<?php

use Illuminate\Database\Seeder;
use App\Book;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // authors holen
        //$authors = App\Author::all()->pluck("id"); // gibt Array von Author-IDs zurück

        // BOOK 1
        $book = new Book;
        $book->title = "Der Herr der Ringe";
        $book->subtitle = "Die Gefährten";
        $book->isbn = "23458101280";
        $book->rating = 9;
        $book->description = "Erster Teil";
        $book->price = "15.99";
        $book->published = new DateTime();
        //associate verbindet buch mit user. kann nur user sein, den en schon gibt
        //map existing user to book
        $book->save();

        $authors = App\Author::all()->get('0');
        // authors hinzufügen (book muss vorher schon gespeichert werden, damit id existiert)
        $book->authors()->sync($authors);
        $book->save();

        $book1 = new Book;
        $book1->title = "Der Herr der Ringe II";
        $book1->subtitle = "Die zwei Türme";
        $book1->isbn = "5463728223";
        $book1->rating = 6;
        $book1->description = "Zweiter Teil";
        $book1->price = "14.99";
        $book1->published = new DateTime();
        $book1->save();

        $book2 = new Book;
        $book2->title = "Der Herr der Ringe III";
        $book2->subtitle = "Die Rückkehr des Königs";
        $book2->isbn = "23523423412";
        $book2->rating = 10;
        $book2->description = "Dritter Teil";
        $book2->price = "15.99";
        $book2->published = new DateTime();
        $book2->save();

        $book1->authors()->sync($authors);
        $book1->save();
        $book2->authors()->sync($authors);
        $book2->save();

        $book3 = new Book;
        $book3->title = "Das Lied von Eis und Feuer";
        $book3->subtitle = "Die Herren von Winterfell";
        $book3->isbn = "5687845345";
        $book3->rating = 10;
        $book3->description = "Erster Teil";
        $book3->price = "14.99";
        $book3->published = new DateTime();
        $book3->save();

        $book4 = new Book;
        $book4->title = "Das Lied von Eis und Feuer II";
        $book4->subtitle = "Das Erbe von Winterfell";
        $book4->isbn = "5687845346";
        $book4->rating = 6;
        $book4->description = "Zweiter Teil";
        $book4->price = "14.99";
        $book4->published = new DateTime();
        $book4->save();

        $book5 = new Book;
        $book5->title = "Das Lied von Eis und Feuer III";
        $book5->subtitle = "Der Thron der Sieben Königreiche";
        $book5->isbn = "5687845347";
        $book5->rating = 7;
        $book5->description = "Dritter Teil";
        $book5->price = "14.99";
        $book5->published = new DateTime();
        $book5->save();

        $book6 = new Book;
        $book6->title = "Das Lied von Eis und Feuer IV";
        $book6->subtitle = "Die Saat des goldenen Löwen";
        $book6->isbn = "5687845348";
        $book6->rating = 8;
        $book6->description = "Vierter Teil";
        $book6->price = "14.99";
        $book6->published = new DateTime();
        $book6->save();

        $book7 = new Book;
        $book7->title = "Das Lied von Eis und Feuer V";
        $book7->subtitle = "Sturm der Schwerter";
        $book7->isbn = "5687845349";
        $book7->rating = 8;
        $book7->description = "Fünfter Teil";
        $book7->price = "14.99";
        $book7->published = new DateTime();
        $book7->save();

        $book8 = new Book;
        $book8->title = "Das Lied von Eis und Feuer VI";
        $book8->subtitle = "Die Königin der Drachen";
        $book8->isbn = "5687845359";
        $book8->rating = 7;
        $book8->description = "Sechster Teil";
        $book8->price = "14.99";
        $book8->published = new DateTime();
        $book8->save();

        $book9 = new Book;
        $book9->title = "Das Lied von Eis und Feuer VII";
        $book9->subtitle = "Zeit der Krähen";
        $book9->isbn = "5687845358";
        $book9->rating = 6;
        $book9->description = "Siebter Teil";
        $book9->price = "14.99";
        $book9->published = new DateTime();
        $book9->save();

        $book10 = new Book;
        $book10->title = "Das Lied von Eis und Feuer VIII";
        $book10->subtitle = "Die dunkle Königin";
        $book10->isbn = "5687845357";
        $book10->rating = 10;
        $book10->description = "Achter Teil";
        $book10->price = "14.99";
        $book10->published = new DateTime();
        $book10->save();

        $book11 = new Book;
        $book11->title = "Das Lied von Eis und Feuer IX";
        $book11->subtitle = "Der Sohn des Greifen";
        $book11->isbn = "5687845356";
        $book11->rating = 7;
        $book11->description = "Neunter Teil";
        $book11->price = "14.99";
        $book11->published = new DateTime();
        $book11->save();

        $book12 = new Book;
        $book12->title = "Das Lied von Eis und Feuer X";
        $book12->subtitle = "Ein Tanz mit Drachen";
        $book12->isbn = "5687845355";
        $book12->rating = 8;
        $book12->description = "Zehnter Teil";
        $book12->price = "14.99";
        $book12->published = new DateTime();
        $book12->save();

        $authors1 = App\Author::all()->get('1');
        $book3->authors()->sync($authors1);
        $book3->save();
        $book4->authors()->sync($authors1);
        $book4->save();
        $book5->authors()->sync($authors1);
        $book5->save();
        $book6->authors()->sync($authors1);
        $book6->save();
        $book7->authors()->sync($authors1);
        $book7->save();
        $book8->authors()->sync($authors1);
        $book8->save();
        $book9->authors()->sync($authors1);
        $book9->save();
        $book10->authors()->sync($authors1);
        $book10->save();
        $book11->authors()->sync($authors1);
        $book11->save();
        $book12->authors()->sync($authors1);
        $book12->save();

        $book13 = new Book;
        $book13->title = "Harry Potter und der Stein der Weisen";
        $book13->isbn = "6574634231";
        $book13->rating = 8;
        $book13->description = "Erster Teil";
        $book13->price = "7.99";
        $book13->published = new DateTime();
        $book13->save();

        $book14 = new Book;
        $book14->title = "Harry Potter und die Kammer des Schreckens";
        $book14->isbn = "6574634232";
        $book14->rating = 8;
        $book14->description = "Zweiter Teil";
        $book14->price = "6.99";
        $book14->published = new DateTime();
        $book14->save();

        $book15 = new Book;
        $book15->title = "Harry Potter und der Gefangene von Askaban";
        $book15->isbn = "6574634233";
        $book15->rating = 10;
        $book15->description = "Dritter Teil";
        $book15->price = "6.99";
        $book15->published = new DateTime();
        $book15->save();

        $book16 = new Book;
        $book16->title = "Harry Potter und der Feuerkelch";
        $book16->isbn = "6574634234";
        $book16->description = "Vierter Teil";
        $book16->price = "7.99";
        $book16->published = new DateTime();
        $book16->save();

        $book17 = new Book;
        $book17->title = "Harry Potter und der Orden des Phönix";
        $book17->isbn = "6574634235";
        $book17->rating = 9;
        $book17->description = "Fünfter Teil";
        $book17->price = "5.99";
        $book17->published = new DateTime();
        $book17->save();

        $book18 = new Book;
        $book18->title = "Harry Potter und die Heiligtümer des Todes";
        $book18->isbn = "6574634236";
        $book18->rating = 8;
        $book18->description = "Sechter Teil";
        $book18->price = "7.99";
        $book18->published = new DateTime();
        $book18->save();

        $book19 = new Book;
        $book19->title = "Harry Potter und der Halbblutprinz";
        $book19->isbn = "6574634237";
        $book19->rating = 8;
        $book19->description = "Siebter Teil";
        $book19->price = "7.99";
        $book19->published = new DateTime();
        $book19->save();

        $authors2 = App\Author::all()->get('2');
        $book13->authors()->sync($authors2);
        $book13->save();
        $book14->authors()->sync($authors2);
        $book14->save();
        $book15->authors()->sync($authors2);
        $book15->save();
        $book16->authors()->sync($authors2);
        $book16->save();
        $book17->authors()->sync($authors2);
        $book17->save();
        $book18->authors()->sync($authors2);
        $book18->save();
        $book19->authors()->sync($authors2);
        $book19->save();

        $book20 = new Book;
        $book20->title = "Paradox";
        $book20->subtitle = "Am Abgrund der Ewigkeit";
        $book20->isbn = "8795676342";
        $book20->rating = 8;
        $book20->description = "Taschenbuch Roman";
        $book20->price = "10";
        $book20->published = new DateTime();
        $book20->save();

        $book21 = new Book;
        $book21->title = "Die Kinder der Zeit";
        $book21->isbn = "8795676343";
        $book21->rating = 8;
        $book21->description = "Taschenbuch Roman";
        $book21->price = "10.99";
        $book21->published = new DateTime();
        $book21->invisible = true;
        $book21->save();

        $authors3 = App\Author::all()->get('4');
        $book21->authors()->sync($authors3);
        $book21->save();

        $book22 = new Book;
        $book22->title = "Das schwarze Schiff";
        $book22->isbn = "8795676341";
        $book22->rating = 1;
        $book22->description = "Taschenbuch";
        $book22->price = "14.99";
        $book22->published = new DateTime();
        $book22->save();

        $authors4 = App\Author::all()->get('3');
        $book20->authors()->sync($authors4);
        $book20->save();
        $book22->authors()->sync($authors4);
        $book22->save();

        $book23 = new Book;
        $book23->title = "Starship";
        $book23->subtitle = "Verloren im Weltraum";
        $book23->isbn = "8795676344";
        $book23->rating = 9;
        $book23->description = "Taschenbuch";
        $book23->price = "13.95";
        $book23->published = new DateTime();
        $book23->save();

        $authors5 = App\Author::all()->get('5');
        $book23->authors()->sync($authors5);
        $book23->save();

        $book24 = new Book;
        $book24->title = "So klingt dein Herz";
        $book24->isbn = "8768732310";
        $book24->rating = 10;
        $book24->description = "Romance Roman";
        $book24->price = "21.95";
        $book24->published = new DateTime();
        $book24->save();

        $book25 = new Book;
        $book25->title = "Ein Moment fürs Leben";
        $book25->isbn = "8768732320";
        $book25->rating = 9;
        $book25->description = "Life Roman";
        $book25->price = "9.95";
        $book25->published = new DateTime();
        $book25->save();

        $authors6 = App\Author::all()->get('6');
        $book24->authors()->sync($authors6);
        $book24->save();
        $book25->authors()->sync($authors6);
        $book25->save();

        $book26 = new Book;
        $book26->title = "Der Ernährungskompass";
        $book26->subtitle = "Das Fazit aller wissenschaftlichen Studien zum Thema Ernährung";
        $book26->isbn = "8768732330";
        $book26->rating = 2;
        $book26->description = "Mit den 12 wichtigsten Regeln der gesunden Ernährung";
        $book26->price = "18.99";
        $book26->published = new DateTime();
        $book26->save();

        $authors7 = App\Author::all()->get('7');
        $book26->authors()->sync($authors7);
        $book26->save();

        $book27 = new Book;
        $book27->title = "Die Stadt der Träumenden Bücher";
        $book27->isbn = "8768732340";
        $book27->rating = 10;
        $book27->description = "Fantasy Roman";
        $book27->price = "12.99";
        $book27->published = new DateTime();
        $book27->save();

        $authors8 = App\Author::all()->get('8');
        $book27->authors()->sync($authors8);
        $book27->save();

        $book28 = new Book;
        $book28->title = "Veröffentlichungen zur Salzburger Musikgeschichte";
        $book28->isbn = "8768732350";
        $book28->description = "Katalog des liturgischen Buch- und Musikalienbestandes am Dom zu Salzburg";
        $book28->price = "130";
        $book28->published = new DateTime();
        $book28->save();



        // IMAGE 1
        $image1 = new App\Image();
        $image1->title = "Cover 1";
        $image1->url = "https://images-na.ssl-images-amazon.com/images/I/51xrz0BOhML._SX311_BO1,204,203,200_.jpg";
        $image1->book()->associate($book);
        $image1->save();

        // IMAGE 2
        $image2 = new App\Image();
        $image2->title = "Neuüberarbeitung";
        $image2->url = "https://images-na.ssl-images-amazon.com/images/I/51Q832vk98L._SX319_BO1,204,203,200_.jpg";
        $image2->book()->associate($book);
        $image2->save();

        $image3 = new App\Image();
        $image3->title = "Cover 2";
        $image3->url = "https://images-na.ssl-images-amazon.com/images/I/512C0K89PTL._SX258_BO1,204,203,200_.jpg";
        $image3->book()->associate($book1);
        $image3->save();

        $image4 = new App\Image();
        $image4->title = "Neuüberarbeitung";
        $image4->url = "https://images-na.ssl-images-amazon.com/images/I/51smfUvItSL._SX319_BO1,204,203,200_.jpg";
        $image4->book()->associate($book1);
        $image4->save();

        $image5 = new App\Image();
        $image5->title = "Cover 3";
        $image5->url = "https://images-na.ssl-images-amazon.com/images/I/51PUM401hBL._SX258_BO1,204,203,200_.jpg";
        $image5->book()->associate($book2);
        $image5->save();

        $image6 = new App\Image();
        $image6->title = "Neuüberarbeitung";
        $image6->url = "https://images-na.ssl-images-amazon.com/images/I/51SmBvM5SBL._SX319_BO1,204,203,200_.jpg";
        $image6->book()->associate($book2);
        $image6->save();

        $image7 = new App\Image();
        $image7->title = "Cover 1";
        $image7->url = "https://images-na.ssl-images-amazon.com/images/I/51oSlaj9tOL._SX313_BO1,204,203,200_.jpg";
        $image7->book()->associate($book3);
        $image7->save();

        $image8 = new App\Image();
        $image8->title = "Cover 2";
        $image8->url = "https://images-na.ssl-images-amazon.com/images/I/51DldjWI-kL._SX350_BO1,204,203,200_.jpg";
        $image8->book()->associate($book3);
        $image8->save();

        $image9 = new App\Image();
        $image9->title = "Cover 1";
        $image9->url = "https://images-na.ssl-images-amazon.com/images/I/51O1zKDDw-L._SX313_BO1,204,203,200_.jpg";
        $image9->book()->associate($book4);
        $image9->save();

        $image10 = new App\Image();
        $image10->title = "Cover 1";
        $image10->url = "https://images-na.ssl-images-amazon.com/images/I/51jgE2ilYaL._SX313_BO1,204,203,200_.jpg";
        $image10->book()->associate($book5);
        $image10->save();

        $image11 = new App\Image();
        $image11->title = "Cover 1";
        $image11->url = "https://images-na.ssl-images-amazon.com/images/I/51V2EMyVfqL._SX313_BO1,204,203,200_.jpg";
        $image11->book()->associate($book6);
        $image11->save();

        $image12 = new App\Image();
        $image12->title = "Cover 1";
        $image12->url = "https://images-na.ssl-images-amazon.com/images/I/51puIAXme1L._SX313_BO1,204,203,200_.jpg";
        $image12->book()->associate($book7);
        $image12->save();

        $image13 = new App\Image();
        $image13->title = "Cover 1";
        $image13->url = "https://images-na.ssl-images-amazon.com/images/I/51fVIoNiKkL._SX313_BO1,204,203,200_.jpg";
        $image13->book()->associate($book8);
        $image13->save();

        $image14 = new App\Image();
        $image14->title = "Cover 1";
        $image14->url = "https://images-na.ssl-images-amazon.com/images/I/51BGBhQLCoL._SX313_BO1,204,203,200_.jpg";
        $image14->book()->associate($book9);
        $image14->save();

        $image15 = new App\Image();
        $image15->title = "Cover 1";
        $image15->url = "https://images-na.ssl-images-amazon.com/images/I/517gSJXBIzL._SX312_BO1,204,203,200_.jpg";
        $image15->book()->associate($book10);
        $image15->save();

        $image16 = new App\Image();
        $image16->title = "Cover 1";
        $image16->url = "https://images-na.ssl-images-amazon.com/images/I/51JCRO9hqJL._SX312_BO1,204,203,200_.jpg";
        $image16->book()->associate($book11);
        $image16->save();

        $image17 = new App\Image();
        $image17->title = "Cover 1";
        $image17->url = "https://images-na.ssl-images-amazon.com/images/I/51L0UmTCZWL._SX312_BO1,204,203,200_.jpg";
        $image17->book()->associate($book12);
        $image17->save();

        $image18 = new App\Image();
        $image18->title = "Cover Deutsch";
        $image18->url = "https://images-na.ssl-images-amazon.com/images/I/51pBjOHuqIL._SX319_BO1,204,203,200_.jpg";
        $image18->book()->associate($book13);
        $image18->save();

        $image19 = new App\Image();
        $image19->title = "Cover Englisch";
        $image19->url = "https://images-na.ssl-images-amazon.com/images/I/51HSkTKlauL._SX346_BO1,204,203,200_.jpg";
        $image19->book()->associate($book13);
        $image19->save();

        $image20 = new App\Image();
        $image20->title = "Cover Deutsch";
        $image20->url = "https://images-na.ssl-images-amazon.com/images/I/51HBXxbREBL._SX319_BO1,204,203,200_.jpg";
        $image20->book()->associate($book14);
        $image20->save();

        $image21 = new App\Image();
        $image21->title = "Cover Englisch";
        $image21->url = "https://images-na.ssl-images-amazon.com/images/I/51F7MMxbhOL._SX324_BO1,204,203,200_.jpg";
        $image21->book()->associate($book14);
        $image21->save();

        $image22 = new App\Image();
        $image22->title = "Cover Deutsch";
        $image22->url = "https://images-na.ssl-images-amazon.com/images/I/51fgtNM-vdL._SX319_BO1,204,203,200_.jpg";
        $image22->book()->associate($book15);
        $image22->save();

        $image23 = new App\Image();
        $image23->title = "Cover Englisch";
        $image23->url = "https://images-na.ssl-images-amazon.com/images/I/51MFYYS59VL._SX323_BO1,204,203,200_.jpg";
        $image23->book()->associate($book15);
        $image23->save();

        $image24 = new App\Image();
        $image24->title = "Cover Deutsch";
        $image24->url = "https://images-na.ssl-images-amazon.com/images/I/51-yHTm2IvL._SX319_BO1,204,203,200_.jpg";
        $image24->book()->associate($book16);
        $image24->save();

        $image25 = new App\Image();
        $image25->title = "Cover Englisch";
        $image25->url = "https://images-na.ssl-images-amazon.com/images/I/516SzpxSeML._SX332_BO1,204,203,200_.jpg";
        $image25->book()->associate($book16);
        $image25->save();

        $image26 = new App\Image();
        $image26->title = "Cover Deutsch";
        $image26->url = "https://images-na.ssl-images-amazon.com/images/I/51ni6oQ7xfL._SX319_BO1,204,203,200_.jpg";
        $image26->book()->associate($book17);
        $image26->save();

        $image27 = new App\Image();
        $image27->title = "Cover Englisch";
        $image27->url = "https://images-na.ssl-images-amazon.com/images/I/51BJ2dEdpQL._SX332_BO1,204,203,200_.jpg";
        $image27->book()->associate($book17);
        $image27->save();

        $image28 = new App\Image();
        $image28->title = "Cover Deutsch";
        $image28->url = "https://images-na.ssl-images-amazon.com/images/I/515N0L7qLsL._SX319_BO1,204,203,200_.jpg";
        $image28->book()->associate($book18);
        $image28->save();

        $image29 = new App\Image();
        $image29->title = "Cover Englisch";
        $image29->url = "https://images-na.ssl-images-amazon.com/images/I/51UGWxhweuL._SX332_BO1,204,203,200_.jpg";
        $image29->book()->associate($book18);
        $image29->save();

        $image30 = new App\Image();
        $image30->title = "Cover Deutsch";
        $image30->url = "https://images-na.ssl-images-amazon.com/images/I/51g05l1J9sL._SX319_BO1,204,203,200_.jpg";
        $image30->book()->associate($book19);
        $image30->save();

        $image31 = new App\Image();
        $image31->title = "Cover Englisch";
        $image31->url = "https://images-na.ssl-images-amazon.com/images/I/5126beRTBUL._SX344_BO1,204,203,200_.jpg";
        $image31->book()->associate($book19);
        $image31->save();

        $image32 = new App\Image();
        $image32->title = "Neues Cover";
        $image32->url = "https://images-na.ssl-images-amazon.com/images/I/515lKF%2BV5-L._SX341_BO1,204,203,200_.jpg";
        $image32->book()->associate($book13);
        $image32->save();

        $image33 = new App\Image();
        $image33->title = "Neues Cover";
        $image33->url = "https://images-na.ssl-images-amazon.com/images/I/61%2BwmElexwL._SX337_BO1,204,203,200_.jpg";
        $image33->book()->associate($book14);
        $image33->save();

        $image34 = new App\Image();
        $image34->title = "Neues Cover";
        $image34->url = "https://images-na.ssl-images-amazon.com/images/I/61jbUQKq2sL._SX337_BO1,204,203,200_.jpg";
        $image34->book()->associate($book15);
        $image34->save();

        $image35 = new App\Image();
        $image35->title = "Neues Cover";
        $image35->url = "https://images-na.ssl-images-amazon.com/images/I/512e0oEUwPL._SX339_BO1,204,203,200_.jpg";
        $image35->book()->associate($book16);
        $image35->save();

        $image36 = new App\Image();
        $image36->title = "Neues Cover";
        $image36->url = "https://images-na.ssl-images-amazon.com/images/I/51wY3FYYSML._SX337_BO1,204,203,200_.jpg";
        $image36->book()->associate($book17);
        $image36->save();

        $image37 = new App\Image();
        $image37->title = "Neues Cover";
        $image37->url = "https://images-na.ssl-images-amazon.com/images/I/51H0SXNhFnL._SX337_BO1,204,203,200_.jpg";
        $image37->book()->associate($book18);
        $image37->save();

        $image38 = new App\Image();
        $image38->title = "Neues Cover";
        $image38->url = "https://images-na.ssl-images-amazon.com/images/I/51K3YbGJESL._SX338_BO1,204,203,200_.jpg";
        $image38->book()->associate($book19);
        $image38->save();

        $image39 = new App\Image();
        $image39->title = "Cover";
        $image39->url = "https://images-na.ssl-images-amazon.com/images/I/41KhADplwHL._SX334_BO1,204,203,200_.jpg";
        $image39->book()->associate($book20);
        $image39->save();

        $image40 = new App\Image();
        $image40->title = "Cover";
        $image40->url = "https://images-na.ssl-images-amazon.com/images/I/51lKdqVBw5L._SX326_BO1,204,203,200_.jpg";
        $image40->book()->associate($book21);
        $image40->save();

        $image41 = new App\Image();
        $image41->title = "Cover";
        $image41->url = "https://images-na.ssl-images-amazon.com/images/I/517RpwKTsLL._SX314_BO1,204,203,200_.jpg";
        $image41->book()->associate($book22);
        $image41->save();

        $image42 = new App\Image();
        $image42->title = "Cover";
        $image42->url = "https://images-na.ssl-images-amazon.com/images/I/41OgTvfQXkL._SX324_BO1,204,203,200_.jpg";
        $image42->book()->associate($book23);
        $image42->save();

        $image43 = new App\Image();
        $image43->title = "Cover";
        $image43->url = "https://images-na.ssl-images-amazon.com/images/I/51gQUssoR-L._SX312_BO1,204,203,200_.jpg";
        $image43->book()->associate($book24);
        $image43->save();

        $image44 = new App\Image();
        $image44->title = "Cover";
        $image44->url = "https://images-na.ssl-images-amazon.com/images/I/51f6XZLVieL._SX327_BO1,204,203,200_.jpg";
        $image44->book()->associate($book25);
        $image44->save();

        $image45 = new App\Image();
        $image45->title = "Cover";
        $image45->url = "https://images-na.ssl-images-amazon.com/images/I/51w1yr32D6L._SX312_BO1,204,203,200_.jpg";
        $image45->book()->associate($book26);
        $image45->save();

        $image46 = new App\Image();
        $image46->title = "Cover";
        $image46->url = "https://images-eu.ssl-images-amazon.com/images/I/61iAgN-A3cL.jpg";
        $image46->book()->associate($book27);
        $image46->save();

        $image47 = new App\Image();
        $image47->title = "Cover";
        $image47->url = "https://images-na.ssl-images-amazon.com/images/I/41AXbS2XRCL._SX329_BO1,204,203,200_.jpg";
        $image47->book()->associate($book28);
        $image47->save();


        $order1 = App\Order::all()->get('0');
        $order2 = App\Order::all()->get('1');
        $order3 = App\Order::all()->get('2');
        $order4 = App\Order::all()->get('3');
        $order5 = App\Order::all()->get('4');
        $order6 = App\Order::all()->get('5');
        $order7 = App\Order::all()->get('6');
        $order8 = App\Order::all()->get('7');
        $order9 = App\Order::all()->get('8');
        $order10 = App\Order::all()->get('9');

        $op1 = new App\Ordersposition();
        $op1->amount = "2";
        $op1->order()->associate($order1);
        $op1->book()->associate($book);
        $op1->save();

        $op2 = new App\Ordersposition();
        $op2->order()->associate($order1);
        $op2->book()->associate($book1);
        $op2->save();

        $op3 = new App\Ordersposition();
        $op3->amount = "5";
        $op3->order()->associate($order1);
        $op3->book()->associate($book12);
        $op3->save();

        $op4 = new App\Ordersposition();
        $op4->amount = "15";
        $op4->order()->associate($order2);
        $op4->book()->associate($book27);
        $op4->save();

        $op5 = new App\Ordersposition();
        $op5->amount = "14";
        $op5->order()->associate($order3);
        $op5->book()->associate($book4);
        $op5->save();

        $op6 = new App\Ordersposition();
        $op6->amount = "1";
        $op6->order()->associate($order4);
        $op6->book()->associate($book28);
        $op6->save();

        $op7 = new App\Ordersposition();
        $op7->amount = "1";
        $op7->order()->associate($order5);
        $op7->book()->associate($book20);
        $op7->save();

        $op8 = new App\Ordersposition();
        $op8->amount = "9";
        $op8->order()->associate($order6);
        $op8->book()->associate($book24);
        $op8->save();

        $op9 = new App\Ordersposition();
        $op9->amount = "2";
        $op9->order()->associate($order7);
        $op9->book()->associate($book22);
        $op9->save();

        $op10 = new App\Ordersposition();
        $op10->order()->associate($order8);
        $op10->book()->associate($book7);
        $op10->save();

        $op11 = new App\Ordersposition();
        $op11->order()->associate($order9);
        $op11->book()->associate($book3);
        $op11->save();

        $op12 = new App\Ordersposition();
        $op12->order()->associate($order10);
        $op12->book()->associate($book11);
        $op12->save();



        /*
        DB::table('books')->insert([
            'title' => str_random(100),
            'isbn' => '123456789',
            'subtitle'=> str_random(100),
            'rating' => 5,
            'description' => str_random(300),
            'published' => new DateTime(),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        */
        /*
                DB::table('books')->insert([
                    'title' => str_random(100),
                    'isbn' => '754456789',
                    'subtitle'=> str_random(100),
                    'rating' => 5,
                    'description' => str_random(300),
                    'published' => new DateTime(),
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]);
        */
    }
}

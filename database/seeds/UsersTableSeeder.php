<?php

use Illuminate\Database\Seeder;
//use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new App\User();
        //$user1 = new User; //() aufruf für constructor
        $user1->lastName = 'testuser';
        $user1->firstName = 'admin';
        $user1->birthday = new DateTime('1990-06-01');
        $user1->email = 'test@gmail.com';
        $user1->admin_flag = true;
        //bcrypt verschlüsselt passwort. muss verschlüsselt gespeichert werden
        //haswert. entschlüsseln nicht möglich
        $user1->password = bcrypt('secret');
        $user1->save();


        $user2 = new App\User;
        $user2->lastName = 'Mustermann';
        $user2->firstName = 'Max';
        $user2->birthday = new DateTime('1987-12-24');
        $user2->email = 'max@gmail.com';
        $user2->password = bcrypt('secret');
        $user2->save();


        $user3 = new App\User;
        $user3->lastName = 'Huber';
        $user3->firstName = 'Helga';
        $user3->birthday = new DateTime('1987-12-24');
        $user3->email = 'hh@gmail.com';
        $user3->password = bcrypt('secret');
        $user3->save();


        $user4 = new App\User;
        $user4->lastName = 'Mayr';
        $user4->firstName = 'Michael';
        $user4->birthday = new DateTime('1987-12-24');
        $user4->email = 'mm@gmail.com';
        $user4->password = bcrypt('secret');
        $user4->save();


        $user5 = new App\User;
        $user5->lastName = 'Strauss';
        $user5->firstName = 'Stefan';
        $user5->birthday = new DateTime('1987-12-24');
        $user5->email = 'stefn@gmail.com';
        $user5->password = bcrypt('secret');
        $user5->save();





        $address1 = new \App\Address();
        $address1->country = 'AT';
        $address1->plz = '4020';
        $address1->location = 'Linz';
        $address1->street = 'Bahnhofstrasse 2';
        $address1->user()->associate($user1);
        $address1->save();

        $address2 = new \App\Address();
        $address2->country = 'DE';
        $address2->plz = '80331';
        $address2->location = 'München';
        $address2->street = 'Bahnhofstrasse 2';
        $address2->user()->associate($user2);
        $address2->save();

        $address3 = new \App\Address();
        $address3->country = 'AT';
        $address3->plz = '1100';
        $address3->location = 'Wien';
        $address3->street = 'Bahnhofstrasse 2';
        $address3->user()->associate($user3);
        $address3->save();

        $address4 = new \App\Address();
        $address4->country = 'AT';
        $address4->plz = '8010';
        $address4->location = 'Graz';
        $address4->street = 'Bahnhofstrasse 2';
        $address4->user()->associate($user4);
        $address4->save();

        $address5 = new \App\Address();
        $address5->country = 'AT';
        $address5->location = 'Salzburg';
        $address5->street = 'Bahnhofstrasse 2';
        $address5->user()->associate($user5);
        $address5->save();
    }
}
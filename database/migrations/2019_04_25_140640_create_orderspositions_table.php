<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderspositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderspositions', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('amount', 2, 0)->default('1');
            $table->integer('book_id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('id')
                ->on('orders')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderspositions');
    }
}

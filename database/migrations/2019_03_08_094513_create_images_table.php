<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('title');
            $table->timestamps();

            //fk field for relation - model name lowercase + "_id"
            //wichtig - per konvention, daran halten, dass orm weiß
            //unsigned heißt ohne vorzeichen. wertebereich für vorzeichen reservier.
            //vorzeichen weglassen -> größerer wertebereich, brauche keinen reservierten speicherbereich
            $table->integer('book_id')->unsigned();
            //foreignkey book_id referenziert auf die tabelle id bei book
            //ondelete cascade -> wenn book gelöscht wird, soll auch image gelöscht werden
            $table->foreign('book_id')
                ->references('id')->on('books')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
